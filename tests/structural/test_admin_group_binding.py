"""
=========================
test_admin_group_bindings
=========================

Verify the roles assigned to the admin group.

Test Plan:

* Get a list of all roles assigned to the admin group
* Check that the desired roles are present in the list

"""

import pytest


@pytest.fixture(scope="module")
def api():
    return "cloudresourcemanager"


@pytest.fixture(scope="module")
def admin_roles(api_client, project_id):
    """Return a list of all roles assigned to the admin group.

    This is a module-scoped fixture to reduce the number of API calls and speed
    up testing.

    :param api_client:
        Cloud Resource Manager API client.
    :param str project_id:
        The current GCP Project ID.

    :returns:
        A list of strings.

    """

    roles = []

    res = api_client.projects().getIamPolicy(
        resource=project_id,
    ).execute()
    for binding in res["bindings"]:
        for member in binding["members"]:
            if not member.endswith("-rhedcloudadministratorrole@gcp.emory.edu"):
                continue

            roles.append(binding["role"])

    return roles


@pytest.mark.parametrize("role", (
    "roles/editor",
    "roles/compute.instanceAdmin.v1",
    "roles/storage.admin",
))
def test_binding(admin_roles, role):
    """Verify the roles assigned to the admin group.

    :param list(str) admin_roles:
        A list of strings, which are the roles assigned to the admin group.
    :param str role:
        The role to verify is assigned to the admin group.

    """

    assert role in admin_roles
