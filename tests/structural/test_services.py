#!/usr/bin/python

import os

import pytest


@pytest.fixture(scope="module")
def sa_scopes():
    return (
        "https://www.googleapis.com/auth/cloud-platform.read-only",
        "https://www.googleapis.com/auth/cloud-platform",
        "https://www.googleapis.com/auth/service.management",
    )


@pytest.fixture(scope="module")
def api():
    return "serviceusage"


@pytest.mark.parametrize("api_url, expected", [
    ("cloudresourcemanager.googleapis.com", "ENABLED"),
    ("iam.googleapis.com", "ENABLED"),
    ("compute.googleapis.com", "ENABLED"),
    ("secretmanager.googleapis.com", "ENABLED"),
    ("cloudtasks.googleapis.com", "DISABLED"),
])
def test_service_exists(api_client, api_url, expected):
    api_path = "projects/{}/services/{}".format(os.getenv("PROJECT_ID"), api_url)
    res = api_client.services().get(
        name=api_path,
    ).execute()

    assert res["state"] == expected
