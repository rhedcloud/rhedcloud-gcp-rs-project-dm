#!/usr/bin/python

from googleapiclient.errors import HttpError
import pytest


@pytest.fixture(scope="module")
def api():
    return "compute"


def test_vpc_fails(api_client, project_id):
    with pytest.raises(HttpError) as exc:
        api_client.networks().insert(
            project=project_id,
            body={
                "name": "test-network",
                "autoCreateSubnetworks": "true",
                "description": "test network",
                "routingConfig": {
                    "routingMode": "REGIONAL",
                },
            },
        ).execute()

    expected_codes = [
        403,  # unauthorized
        409,  # VPC already exists
    ]

    assert exc.value.resp.status in expected_codes, exc
