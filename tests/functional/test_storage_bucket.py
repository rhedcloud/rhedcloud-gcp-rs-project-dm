#!/usr/bin/python

import os

from google.api_core.exceptions import NotFound
from google.cloud import storage
import pytest


@pytest.fixture(scope="module")
def client(sa_creds):
    return storage.Client(credentials=sa_creds)


@pytest.fixture
def bucket_name(scope="module"):
    return os.getenv("GCS_BUCKET") or "blah-34563"


def test_create_bucket(client, bucket_name):
    """Verify access to create storage buckets.

    :param client:
        Storage API Client.
    :param str bucket_name:
        Name of the storage bucket to attempt to create.

    """

    bucket = client.create_bucket(bucket_name)

    assert isinstance(bucket, storage.Bucket)
    assert bucket.name == bucket_name


def test_delete_bucket(client, bucket_name):
    """Verify access to delete storage buckets.

    :param client:
        Storage API Client.
    :param str bucket_name:
        Name of the storage bucket to attempt to delete.

    """

    bucket = client.get_bucket(bucket_name)

    assert isinstance(bucket, storage.Bucket)
    bucket.delete()

    # we expect the bucket to have been deleted above, so we should not be able
    # to get it a second time.
    with pytest.raises(NotFound):
        bucket = client.get_bucket(bucket_name)
