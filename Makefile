MARK ?= not slowtest
KEYWORD ?= test
ARGS ?=

test: activate-service-account
	pytest -m "$(MARK)" -k "$(KEYWORD)" $(ARGS)

slowtest:
	$(MAKE) MARK='slowtest' test

# ----------------------------------------------------------------------------
# The following variables and targets are present to try to clean up and
# recreate the pipeline script in Bitbucket.
# ----------------------------------------------------------------------------

# These variables may be overridden by environment variables with the same name
REPO_NAME ?= rhedcloud-gcp-rs-project-dm
PROJECT_ID ?= gcp-pipeline-project-1
PROJECT_NUMBER ?= 919644137355
DEPLOYMENT_NAME ?= rhedcloud-gcp-rs-project
BITBUCKET_BUILD_NUMBER ?= 0
GOOGLE_APPLICATION_CREDENTIALS ?= /tmp/key_file

save-creds:
	save_api_keys.sh

activate-service-account:
	gcloud auth activate-service-account --key-file "$(GOOGLE_APPLICATION_CREDENTIALS)"
	gcloud config set project $(PROJECT_ID)
	$(MAKE) whoami

whoami:
	gcloud config list account --format "value(core.account)"

# Retrieve necessary artifacts and run a sequence of targets to clean and
# rebuild our stacks.
rebuild: clean rs-project

# Run a sequence of targets to clean and rebuild our stacks.
local-rebuild: clean rs-project

# Clean up resources if stack changes are detected
clean: activate-service-account
	delete_deployment.py "$(DEPLOYMENT_NAME)"

# Create the rs-project deployment
rs-project: activate-service-account working-rs-project
	cat "$(DEPLOYMENT_NAME)-dm-working.json"
	gcloud deployment-manager deployments create "$(DEPLOYMENT_NAME)" \
		--project "$(PROJECT_ID)" \
		--config "$(DEPLOYMENT_NAME)-dm-working.json"

# Replace various placeholders in the deployment template with the appropriate values.
working-rs-project:
	sed -e "s/BITBUCKET_BUILD_NUMBER/$(BITBUCKET_BUILD_NUMBER)/g" \
		-e "s/PROJECT_ID/$(PROJECT_ID)/g" \
		-e "s/PROJECT_NUMBER/$(PROJECT_NUMBER)/g" \
		"$(DEPLOYMENT_NAME)-dm.json" > "$(DEPLOYMENT_NAME)-dm-working.json"

# Upload test artifacts
upload:
	git archive --format zip --output $(REPO_NAME).latest.zip master
	cp $(REPO_NAME).json $(REPO_NAME).latest.json
	upload_artifact.sh $(REPO_NAME).latest.json $(REPO_NAME).latest.zip

.EXPORT_ALL_VARIABLES:
