# rhedcloud-aws-rs-account-cfn

RHEDcloud GCP Deployment Manager Template for project setup (ex. roles, policies, and groups).

## Parameters

* **AddHIPAAIAMPolicy**: Create IAM policy for HIPAA AWS accounts.  Set to Yes for HIPAA accounts.
* **CloudTrailName**: Name given to the centrally managed s3 bucket (e.g. rhedcloud-aws-admin3-ct1).
* **RHEDcloudAwsAccountServiceUserArn**: The ARN of the AwsAccountService user that is given cross-account access.
* **RHEDcloudIDP**: The name of the SAML IDP in the account.
* **RHEDcloudMaintenanceOperatorRoleArn**: The ARN of the RHEDcloudMaintenanceOperatorRole role that is given cross-account access.
* **RHEDcloudSamlIssuer**: The identifier of the SAML IDP.
* **RHEDcloudSecurityRiskDetectionServiceUserArn**: The ARN of the SecurityRiskDetectionService user that is given cross-account access.

## Resources

* **AccountCloudTrailBucket**: This is a central bucket, which is read-only for customers admnistrators.  Originally, the main purpose of this bucket was to store CloudTrail logs, but the advent of Organizational CloudTrail alleviated the need to have a separate CloudTrail in each account.  This bucket is still used as a temporary store for service account credentials as requested.
* **AccountCloudTrailBucketPolicy**: The Cloud Trail service needs permission to write to the desired bucket. This policy grants that access. This policy needs to be updated since Organizational CloudTrails were implemented.
* **RHEDcloudAdministratorRole**: The RHEDcloudAdministratorRole must allow all operations RHEDcloud customer administrators need to perform as restricted administrators of the account. It must explicitly prohibit operations deemed inappropriate by Information Security and any operations that follow from the aforementioned prohibitions. Most restrictions are implemented as service control policies at the organization level. However restrictions that cannot be implemented as service control policies are implemented as managed policies, which are attached to this role. Presently there is two (2) policies that gets attached to the RHEDcloudAdministratorRole: AdministratorAccess and RHEDcloudAdministratorRolePolicy
* **RHEDcloudAdministratorRoleHipaaPolicy**: This policy prevents customer administrators from creating ineligible HIPAA resources such as RDS database engines.
* **RHEDcloudAdministratorRolePolicy**: This policy prevents iam operations on roles that should not be modifiable by customer administrators, but rather only central administrators. The policy should also prevent operations on policies that are attached to these roles.
* **RHEDcloudAuditorRole**: This role gives individuals such as University auditors and other appropriate auditors read-only access to AWS accounts using the AWS managed policy for read-only access.
* **RHEDcloudAwsAccountServiceRole**: The RHEDcloudAwsAccountServiceRole is assumed by applications implementing AwsAccountService. The AwsAccountService is the service responsible for setting up and managing accounts created as part of the RHEDcloud AWS service.
* **RHEDcloudAwsAccountServiceRolePolicy**: Presently this policy grants administrator access, which is itself only limited by the service control policies, but the goal is to implement a restrictive policy, specific to the actions the security checker applications must perform.
* **RHEDcloudCentralAdministratorRole**: The RHEDcloudCentralAdministratorRole should allow all operations possible to enable central administration of accounts. Presently Information Security has not articulated any specific restrictions that are required for this role, so we may grant access using any appropriate method to achieve necessary access to these accounts to perform administrative functions. Note, however, that this role like all roles in these accounts will be limited by the overarching service control policies implemented at the organization level, which deny access to restricted AWS services and specific AWS service operations.
* **RHEDcloudMaintenanceOperatorRole**: The RHEDcloudMaintenanceOperatorRole is assumed by Cloud Administrators who need to maintain accounts. The RHEDcloudMaintenanceOperatorRole is configured to be assumed by users who have assumed the RHEDcloudMaintenanceOperatorRole in the master account.
* **RHEDcloudSecurityIRRole**: The RHEDcloudSecurityIRRole should allow all operations required for security incident response.  This role will be used by Enterprise Security team to respond to secuirty incidents.
* **RHEDcloudSecurityIRRolePolicy**: Presently this policy grants the RHEDcloudSecurityIRRole access to only those services a Security Administrator needs to perform their job duties.
* **RHEDcloudSecurityRiskDetectionServiceRole**: The RHEDcloudSecurityRiskDetectionServiceRole is assumed by applications implementing security checking and remediation functions. Security checkers detect and optionally remedy security policy violations such as unencrypted EBS volumes, publicly accessible S3 buckets, strong password policy, unused credentials, etc.
* **RHEDcloudSecurityRiskDetectionServiceRolePolicy**: Presently this policy grants administrator access, which is itself only limited by the service control policies, but the goal is to implement a restrictive policy, specific to the actions the security checker applications must perform.
* **RHEDcloudServiceAccountGroup**: The RHEDcloudServiceAccountGroup is an IAM group used to hold service account users.  This group will have the Administrator role policy and the subnet policies attached to it.
* **RHEDcloudVpcFlowLogRole**: This role gives the AWS VPC flow logs service permission to read and write logs.
* **RHEDcloudVpcFlowLogRolePolicy**: VPC Flow logs contain networking information necessary for troubleshooting and security forensics.  This policy allows the AWS Flow Log service permission to write flow logs to the CloudWatch service.
